---
title: "Front Matter"
subtitle: "Use front matter to add metadata."
description: "Use front matter to add metadata to your content."
categories:
  - "simplicity"
date: 2024-05-02T15:50:24Z
type: post
---

The front matter at the top of each content file is metadata that:

* Describes the content
* Augments the content
* Establishes relationships with other content
* Controls the published structure of your site
* Determines template selection

See [All Reserved Fields](https://gohugo.io/content-management/front-matter/#fields).

## Basic Fields

title
: The page title.
: type: string

description
: Conceptually different than the page summary, the description is typically rendered within a meta element within the head element of the published HTML file.
: type: string

keywords
: An array of keywords, typically rendered within a meta element within the head element of the published HTML file, or used as a taxonomy to classify content.
: type: array of string

images
: List of image locations for opengraph and blog post image.
: type: array of string

date
: The date associated with the page, typically the creation date.
: type: string

lastmod
: The date that the page was last modified.
: type: string

type
: The content type, overriding the value derived from the top level section in which the page resides.
: type: string

draft
: If true, the page will not be rendered unless you pass the --buildDrafts flag to the hugo command.
: type: boolean

## Page Params

seoTitle
: The title for SEO purpose.
: type: string

subtitle
: The page subtitle.
: type: string

style
: Overwrite site style for the associated page.
: type: [style schema](/simplicity/schemas#style-schema)

leaderboard
: Overwrite site leaderboard banner for the associated page.
: type: [leaderboard schema](/simplicity/schemas#leaderboard-schema)

offers:
: Append medium rectangle banner.
: type: array of [offer schema](/simplicity/schemas#offer-schema)

categories
: An array of categories, typically used as a taxonomy to classify content.
: type: array of string

tags
: An array of tags, typically used as a taxonomy to classify content.
: type: array of string
