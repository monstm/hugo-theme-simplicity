---
title: "Shortcodes"
subtitle: "Simple snippets inside your content files calling built-in or custom templates."
description: "Shortcodes are simple snippets inside your content files calling built-in or custom templates."
categories:
  - "simplicity"
date: 2024-05-27T20:08:50Z
type: post
---

## Gallery

### Code block

```md
{{</* gallery */>}}
    {{</* gallerItem image="https://picsum.photos/300/200.webp?random=11" title="title 1" text="text 1" */>}}
    {{</* gallerItem image="https://picsum.photos/300/200.webp?random=12" title="title 2" text="text 2" */>}}
    {{</* gallerItem image="https://picsum.photos/300/200.webp?random=13" title="title 3" text="text 3" */>}}
    {{</* gallerItem image="https://picsum.photos/300/200.webp?random=14" title="title 4" text="text 4" */>}}
    {{</* gallerItem image="https://picsum.photos/300/200.webp?random=15" title="title 5" text="text 5" */>}}
    {{</* gallerItem image="https://picsum.photos/300/200.webp?random=16" title="title 6" text="text 6" */>}}
    {{</* gallerItem image="https://picsum.photos/300/200.webp?random=17" title="title 7" text="text 7" */>}}
{{</* /gallery */>}}
```

### gallerItem Params

image
: Image location.
: type: string

title
: Image title.
: type: string

text
: Image text.
: type: string

### Html Rendered

{{< gallery >}}
    {{< gallerItem image="https://picsum.photos/300/200.webp?random=11" title="title 1" text="text 1" >}}
    {{< gallerItem image="https://picsum.photos/300/200.webp?random=12" title="title 2" text="text 2" >}}
    {{< gallerItem image="https://picsum.photos/300/200.webp?random=13" title="title 3" text="text 3" >}}
    {{< gallerItem image="https://picsum.photos/300/200.webp?random=14" title="title 4" text="text 4" >}}
    {{< gallerItem image="https://picsum.photos/300/200.webp?random=15" title="title 5" text="text 5" >}}
    {{< gallerItem image="https://picsum.photos/300/200.webp?random=16" title="title 6" text="text 6" >}}
    {{< gallerItem image="https://picsum.photos/300/200.webp?random=17" title="title 7" text="text 7" >}}
{{< /gallery >}}