---
title: "Schemas"
subtitle: "List of all simplicity schamas."
description: "List of all simplicity schamas."
categories:
  - "simplicity"
date: 2024-05-03T16:47:32Z
type: post
---

## Style Schema

backgroundImage
: The CSS background image location property.
: type: string
: default: https://cdn.jsdelivr.net/npm/theme-simplicity@2/image/background.webp

backgroundColor
: The CSS background color property.
: type: string
: default: #fff

primaryColor
: The CSS primary color property.
: type: string
: default: #e64946

secondaryColor
: The CSS secondary color property.
: type: string
: default: #2a2a2a

borderColor
: The CSS border color property.
: type: string
: default: #ebebeb

searchboxBackgroundColor
: The CSS background color property for search box.
: type: string
: default: #f5f5f5

footerTextColor
: The CSS text color property for footer section.
: type: string
: default: #999999

footerBorderColor
: The CSS border color property for footer section.
: type: string
: default: rgba(255, 255, 255, 0.3)

postBorderColor
: The CSS border color property for post section.
: type: string
: default: #000

postTableHoverColor
: The CSS table hover color property for post section.
: type: string
: default: rgba(0, 0, 0, 0.1)

postTableOddColor
: The CSS table odd row color property for post section.
: type: string
: default: rgba(0, 0, 0, 0.05)

postBlockquoteBackgroundColor
: The CSS blockquote background color property for post section.
: type: string
: default: rgba(0, 0, 0, 0.1)

## Leaderboard Schema

link
: The leaderboard link location.
: type: string

desktop
: The leaderboard image location for desktop. 728x90
: type: string

mobile
: The leaderboard image location for mobile. 320x50
: type: string

text
: The leaderboard text.
: type: string

## Offer Schema

link
: The offer link location.
: type: string

image
: The offer image location. 350x250
: type: string

text
: The offer text.
: type: string

## Icon Schema

link
: The icon link location.
: type: string

icon
: The icon name.
: type: string
: options: email, phone, facebook, instagram, linkedin, pinterest, twitter, tumblr, youtube, bitbucket, github, gitlab, android, apple

text
: The icon text.
: type: string

## Anchor Schema

link
: The anchor link location.
: type: string

text
: The anchor text.
: type: string

target
: The anchor target.
: type: string

rel
: The anchor relationship.
: type: string

## Column Schema

title
: The column title.
: type: string

items
: The column items.
: type: array of [anchor schema](#anchor-schema)
