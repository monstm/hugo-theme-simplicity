---
title: "Site Configuration"
subtitle: "How to configure your website."
description: "How to configure your website."
categories:
  - "simplicity"
date: 2024-05-01T08:40:42Z
type: post
---

Create a site configuration file in the root of your project directory, naming it **hugo.toml**, **hugo.yaml**, or **hugo.json**, with that order of precedence. See [All Configuration Settings](https://gohugo.io/getting-started/configuration/#all-configuration-settings).

## Basic Configuration

baseURL
: The absolute URL (protocol, host, path, and trailing slash) of your published site.
: type: string
: example: <https://monstm.gitlab.io/theme-simplicity/html/>

title
: Site title.
: type: string

theme
: The directory name for the module as stored in your themes folder.
: type: string

defaultContentLanguage
: Content without language indicator will default to this language.
: type: string
: default: en
: options: bg, cs, da, de, el, en-GB, en-US, en, es, et, fi, fr, hu, id, it, ja, ko, lt, lv, nb, nl, pl, pt-BR, pt-PT, ro, ru, sk, sl, sv, tr, uk, zh

## Site Params

favicon
: Favicon location.
: type: string

style
: The site style.
: type: [style schema](/simplicity/schemas#style-schema)

tagline
: The site tagline.
: type: string

recentPosts
: Size of recent posts.
: type: integer
: default: 10

relatedPosts
: Size of related posts.
: type: integer
: default: 10

leaderboard
: Leaderboard banner.
: type: [leaderboard schema](/simplicity/schemas#leaderboard-schema)

offers:
: An array of medium rectangle banner.
: type: array of [offer schema](/simplicity/schemas#offer-schema)

homeShortcut
: Home shortcut position, available for mobile display only.
: type: string
: options: top, middle, bottom

shortcuts
: An array of shortcut navigations.
: type: array of [icon schema](/simplicity/schemas#icon-schema)

navbarTop
: An array of top navigation.
: type: array of [anchor schema](/simplicity/schemas#anchor-schema)

navbarMiddle
: An array of middle navigation.
: type: array of [anchor schema](/simplicity/schemas#anchor-schema)

navbarBottom
: An array of bottom navigation.
: type: array of [anchor schema](/simplicity/schemas#anchor-schema)

footerColumns
: An array of bottom navigation.
: type: array of [column schema](/simplicity/schemas#column-schema)
