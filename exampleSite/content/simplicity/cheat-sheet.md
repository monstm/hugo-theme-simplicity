---
title: "Cheat Sheet"
subtitle: "Provides a quick overview of all the Markdown syntax elements."
description: "Markdown cheat sheet provides a quick overview of all the Markdown syntax elements."
categories:
  - "simplicity"
date: 2024-05-05T14:43:12Z
type: post
---

This Markdown cheat sheet provides a quick overview of all the Markdown syntax elements. It can't cover every edge case, so if you need more information about any of these elements, refer to the reference guides for [basic syntax](https://www.markdownguide.org/basic-syntax/) and [extended syntax](https://www.markdownguide.org/extended-syntax/).

## Headings

## Heading level 2

### Heading level 3

#### Heading level 4

##### Heading level 5

###### Heading level 6

## Paragraphs

This is the first line.
And this is the second line.

Don't put tabs or spaces in front of your paragraphs.

Keep lines left-aligned like this.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec nisl orci. Praesent hendrerit ac ex non faucibus. Curabitur rutrum lobortis enim quis rutrum. Integer ac nulla aliquam, vulputate erat aliquam, ultrices nunc. Aenean ac maximus velit, eget semper eros. Sed facilisis ligula vel laoreet venenatis. Nullam maximus libero semper volutpat finibus. Nam viverra magna quis ornare dictum. Nam ac luctus nisl. Pellentesque sed lobortis quam. Integer in laoreet erat. Suspendisse consequat, risus id interdum ullamcorper, justo purus tincidunt mauris, eget commodo odio tortor eget nibh.

Quisque nec rhoncus sem. Quisque quis risus vel dui porta varius non non erat. Morbi euismod venenatis dictum. Integer sem felis, aliquet ac risus vitae, accumsan faucibus lacus. Sed vitae viverra eros. Pellentesque eget est porta, viverra felis sed, pellentesque tortor. Vivamus hendrerit ante a tempus sollicitudin. Quisque feugiat dolor mi, ut porttitor nisi consequat ut. Ut iaculis ipsum nec sagittis venenatis. Donec posuere quis justo at consequat.

## Sentences

Sentence with *italic text*.

Sentence with **bold text**.

Sentence with ***italic bold text***.

Sentence with ~~strikethrough text~~.

Sentence with `code text`.

Sentence with [Link text](# "Link Title").

## Table

| Title 1  | Title 2  | Title 3  |  Title 4 |
| -------- | :------- | :------: | -------: |
| Data 1 1 | Data 2 1 | Data 3 1 | Data 4 1 |
| Data 1 2 | Data 2 2 | Data 3 2 | Data 4 2 |

## Blockquote

### Simple Blockquote

> Dorothy followed her through many of the beautiful rooms in her castle.

### Nested Blockquote

> Dorothy followed her through many of the beautiful rooms in her castle.
>
>> The Witch bade her clean the pots and kettles and sweep the floor and keep the fire fed with wood.

## Horizontal Rule

Try to put a blank line before...

---

...and after a horizontal rule.

## List

* First item unordered list
* Second item unordered list
  1. Indented first ordered item
  2. Indented second ordered item
* Third item unordered list
  * Indented first unordered item
  * Indented second unordered item
* Fourth item unordered list

1. First item ordered list
2. Second item ordered list
   1. Indented first ordered item
   2. Indented second ordered item
3. Third item ordered list
   * Indented first unordered item
   * Indented second unordered item
4. Fourth item ordered list

* [x] First item checklist
* [x] Second item checklist
* [ ] Third item checklist

First Term
: First definition of the first term.

Second Term
: First definition of the second term.
: Second definition of the second term.

## Footnote

First sentence with a footnote. [^1]

Second sentence with a footnote. [^2]

Third sentence with a footnote. [^3]

[^1]: First footnote.
[^2]: Second footnote.
[^3]: Third footnote.

## Images

![Image Description](https://picsum.photos/300/200.webp?random=1)

![Image Description](https://picsum.photos/300/200.webp?random=2 "Image Title")

[![Image Description](https://picsum.photos/300/200.webp?random=3 "Image Title")](#)

[![Image Description](https://picsum.photos/300/200.webp?random=4 "Image Title")](# "Link Title")

## Code Block

```txt
This is a code block.
```

```js
console.log('Hello');
console.log('World');
```
