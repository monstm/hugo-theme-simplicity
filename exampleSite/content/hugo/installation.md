---
title: "Installation"
subtitle: "Several ways to install hugo."
description: "Hugo has several ways to install on your computer."
categories:
  - "hugo"
date: 2024-04-29T17:21:11Z
type: post
---

There are several ways to install hugo on your computer.
Hugo can be installed on macOS, Linux, Windows, BSD, and on any machine that can run the Go compiler tool chain.
And here, you will find another way to do it. The fact is, you can even use hugo without installing it.

## Get Docker

Docker is an open platform for developing, shipping, and running applications.

Docker allows you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications.

By taking advantage of Docker's methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

You can download and install Docker on multiple platforms. Refer to the following section and choose the best installation path for you.

* [Docker Desktop for Mac](https://docs.docker.com/desktop/install/mac-install/ "A native application using the macOS sandbox security model that delivers all Docker tools to your Mac.")
* [Docker Desktop for Windows](https://docs.docker.com/desktop/install/windows-install/ "A native Windows application that delivers all Docker tools to your Windows computer.")
* [Docker Desktop for Linux](https://docs.docker.com/desktop/install/linux-install/ "A native Linux application that delivers all Docker tools to your Linux computer.")

## Installing Git

Before you start using Git, you have to make it available on your computer. Even if it's already installed, it's probably a good idea to update to the latest version. You can either install it as a package or via another installer, or download the source code and compile it yourself.

### Installing Git on Linux

If you want to install the basic Git tools on Linux via a binary installer, you can generally do so through the package management tool that comes with your distribution. If you're on Fedora (or any closely-related RPM-based distribution, such as RHEL or CentOS), you can use `dnf`:

```sh
sudo dnf install git-all
```

If you're on a Debian-based distribution, such as Ubuntu, try `apt`:

```sh
sudo apt install git-all
```

For more options, there are instructions for installing on several different Unix distributions on the Git website, at <https://git-scm.com/download/linux>.

### Installing Git on macOS

There are several ways to install Git on macOS. The easiest is probably to install the Xcode Command Line Tools. On Mavericks (10.9) or above you can do this simply by trying to run `git` from the Terminal the very first time.

```sh
git --version
```

If you don't have it installed already, it will prompt you to install it.

If you want a more up to date version, you can also install it via a binary installer. A macOS Git installer is maintained and available for download at the Git website, at <https://git-scm.com/download/mac>.

![Git macOS installer](https://git-scm.com/book/en/v2/images/git-osx-installer.png)

### Installing Git on Windows

There are also a few ways to install Git on Windows. The most official build is available for download on the Git website. Just go to <https://git-scm.com/download/win> and the download will start automatically. Note that this is a project called Git for Windows, which is separate from Git itself; for more information on it, go to <https://gitforwindows.org>.

To get an automated installation you can use the [Git Chocolatey package](https://community.chocolatey.org/packages/git). Note that the Chocolatey package is community maintained.

## Hugo Official Guide

Install Hugo on macOS, Linux, Windows, BSD, and on any machine that can run the Go compiler tool chain.

* [macOS](https://gohugo.io/installation/macos/ "Install Hugo on macOS.")
* [Linux](https://gohugo.io/installation/linux/ "Install Hugo on Linux.")
* [Windows](https://gohugo.io/installation/windows/ "Install Hugo on Windows.")
* [BSD](https://gohugo.io/installation/bsd/ "Install Hugo on BSD derivatives.")
