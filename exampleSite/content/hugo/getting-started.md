---
title: "Getting Started"
subtitle: "how to setup HUGO without installing a local version of HUGO."
description: "how to setup HUGO without installing a local version of HUGO on your machine."
categories:
  - "hugo"
date: 2024-04-30T19:28:31Z
type: post
---

In this article, I'm going to describe how to set up HUGO without installing a local version of HUGO on your machine by using a Docker image instead.

When choosing a specific Docker image, be aware that HUGO comes in two editions: standard and extended. The extended edition supports Sass and encoding WebP images; it is recommended by the HUGO team to use the extended edition, and that's what we will do in this article.

We will be using a Docker image from [klakegg repository](https://hub.docker.com/r/klakegg/hugo). All Docker images containing ext in their name use the extended edition of HUGO. We will use the Alpine-based image.

The Docker image contains the HUGO CLI and exposes the Hugo command as the `ENTRYPOINT`. Running the container and specifying the arguments for the Hugo command gives you all the options you would have when you have HUGO installed locally.

For example, the following command would show the version of HUGO when HUGO is installed locally:

```sh
hugo version
```

The same command can be run using the Docker container:

```sh
docker run --rm klakegg/hugo:ext-alpine version
```

> The `--rm` flag indicates to automatically remove the container when it exits.

## Useful Commands

### Create New Site

```sh
docker run --rm -v .:/src klakegg/hugo:ext-alpine new site site-name --format yaml
```

> The `-v` flag indicates to bind mount a volume.

### Create New Content

```sh
docker run --rm -v .:/src klakegg/hugo:ext-alpine new content-name.md
```

### Build-in Server

```sh
docker run --rm -v .:/src -p 1313:1313 klakegg/hugo:ext-alpine server -D -E -F --poll 3s
```

Or, use command below if port 1313 is blocked

```sh
docker run --rm -v .:/src -p 8080:8080 klakegg/hugo:ext-alpine server -D -E -F -p 8080 --poll 3s
```

> The `-p` flag indicates to publish a container's port(s) to the host.

### Build Website

```sh
docker run --rm -v .:/src klakegg/hugo:ext-alpine --gc --cleanDestinationDir --minify
```

## Theme Installation

### Initiate GIT Project

```sh
git init
```

### Add Theme As A Submodule

```sh
git submodule add https://gitlab.com/monstm/hugo-theme-simplicity.git themes/simplicity
```

### Update Submodule

```sh
git submodule update --init --recursive --remote
```
