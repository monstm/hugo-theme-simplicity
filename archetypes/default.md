---
slug: {{ .Name | urlize }}
title: {{ replace .Name "-" " " | title }}
seoTitle: {{ replace .Name "-" " " | title }}
subtitle: ""
description: ""
keywords:
  - ""
images:
  - ""
style:
  backgroundImage: ""
  backgroundColor: ""
  primaryColor: ""
  secondaryColor: ""
  borderColor: ""
  searchboxBackgroundColor: ""
  footerTextColor: ""
  footerBorderColor: ""
  postBorderColor: ""
  postTableHoverColor: ""
  postTableOddColor: ""
  postBlockquoteBackgroundColor: ""
leaderboard:
  link: ""
  desktop: ""
  mobile: ""
  text: ""
offers:
  - link: ""
    image: ""
    text: ""
categories:
  - ""
tags:
  - ""
date: {{ .Date }}
lastmod: {{ .Date }}
type: post
draft: true
---

