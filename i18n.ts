import 'dotenv/config'
import { SourceLanguageCode, TargetLanguageCode, TranslateTextOptions, Translator } from 'deepl-node'
import { readFileSync, writeFileSync } from 'fs'
import yaml from 'js-yaml'

const I18N_LANGUAGE: SourceLanguageCode = 'en'
const I18N_FILE = './i18n.json'

const deeplApiKey = process.env?.DEEPL_API_KEY
if (!deeplApiKey) {
  throw new Error('Missing DEEPL_API_KEY environment variable')
}
const translator = new Translator(deeplApiKey)

const getLanguages = async (): Promise<string[]> => {
  const targetLanguages = await translator.getTargetLanguages()
  return targetLanguages.map((language) => language.code)
}

const translateTexts = async (
  sourceLang: string,
  targetLang: string,
  data: Record<string, string>
): Promise<Record<string, string>> => {
  if (sourceLang === targetLang) {
    return data
  }

  const keys = Object.keys(data)
  const values = Object.values(data)
  const options: TranslateTextOptions = { tagHandling: 'html' }
  const translations = await translator.translateText(
    values,
    sourceLang as SourceLanguageCode,
    targetLang as TargetLanguageCode,
    options
  )

  return keys.reduce((result, key, index) => ({ ...result, [key]: translations[index].text }), {})
}

const i18n = async (language: string, data: Record<string, string>): Promise<void> => {
  console.log(`Generating ${language}...`)

  const filename = `./i18n/${language}.yaml`
  const translation = await translateTexts(I18N_LANGUAGE, language, data)
  const content = yaml.dump(translation)
  writeFileSync(filename, content, 'utf8')
}

;(async () => {
  const data = JSON.parse(readFileSync(I18N_FILE, 'utf-8'))
  await i18n(I18N_LANGUAGE, data)

  const languages = await getLanguages()
  for (const language of languages) {
    await i18n(language, data)
  }
})()
