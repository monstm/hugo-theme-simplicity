# Hugo Theme Simplicity

live demo: <https://hugo-theme-simplicity.netlify.app/>

## Prerequisites

* [Docker](https://docs.docker.com/get-docker/)
* [GIT](https://git-scm.com/downloads/)
* [NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

## Development

### Create New Content

```sh
docker run --rm -v .:/src klakegg/hugo:ext-alpine new content-title.md -c exampleSite/content
```

### Build Site

```sh
docker run --rm -v .:/src klakegg/hugo:ext-alpine -c exampleSite/content --config exampleSite/hugo.yaml --gc --cleanDestinationDir --minify
```

### HTML Validation

```sh
npm run html-validate
```

### Build-In Server

```sh
docker run --rm -v .:/src -p 1313:1313 klakegg/hugo:ext-alpine server -D -E -F -c exampleSite/content --config exampleSite/hugo.yaml --poll 3s
```

Or, use command below if port 1313 is blocked

```sh
docker run --rm -v .:/src -p 8080:8080 klakegg/hugo:ext-alpine server -D -E -F -p 8080 -c exampleSite/content --config exampleSite/hugo.yaml --poll 3s
```
